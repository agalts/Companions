﻿using Companions.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Companions.Models.Updates;

using Microsoft.Data.SqlClient;
using System;
using System.Data;
using System.Threading.Tasks;

using Companions.Models.Database;

namespace Companions.Controllers
{
    public class ListController : Controller
    {
        // GET: ListController
        public ActionResult Index()
        {
            Updates u = new Updates();
            ViewBag.N = u.CheckNotifications(User.Identity.Name);
            ViewBag.M = u.CheckMessages(User.Identity.Name);
            string sqlExpression = "SELECT * FROM Suggestions";

            List<Suggestions> l = new List<Suggestions>();

            try
            {
                using (SqlConnection connection = new SqlConnection(Database.connectionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand(sqlExpression, connection);
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows) // если есть данные
                    {

                        while (reader.Read())
                        {
                            Suggestions item = new Suggestions();
                            item.id = Convert.ToInt32(reader.GetValue(0));
                            item.departureCity = reader.GetValue(3).ToString();
                            item.arrivalCity = reader.GetValue(4).ToString();
                            if (reader.GetValue(5) != DBNull.Value) item.ArrivalDestinationDate = reader.GetValue(5).ToString();
                            if (reader.GetValue(6) != DBNull.Value) item.ArrivalDepartureDate = reader.GetValue(6).ToString();
                            if (reader.GetValue(7) != DBNull.Value) item.CountOfCompanions = Convert.ToInt32(reader.GetValue(7));
                            item.UserPublished = reader.GetValue(1).ToString();
                            if (reader.GetValue(10) != DBNull.Value) item.TravelCost = Convert.ToInt32(reader.GetValue(10));
                            if (reader.GetValue(8) != DBNull.Value) item.AlreadySentApplication = Convert.ToInt32(reader.GetValue(8));
                            if (reader.GetValue(9) != DBNull.Value) item.Automobile = reader.GetValue(9).ToString();

                            if (Convert.ToBoolean(reader.GetValue(2)))
                                item.Traveller = "Попутчик";
                            else
                                item.Traveller = "Путешественник";

                            l.Add(item);
                        }
                    }
                    reader.Close();
                    return View(l);
                }
            }

            catch
            {
                Response.Redirect("/Error/Exception");

                return View();
            }

        }
    }
}
