﻿using Companions.Models;
using Companions.Models.Updates;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Microsoft.Data.SqlClient;
using System;
using System.Data;
using System.Threading.Tasks;
using Companions.Models.Updates;
using Companions.Models.Database;

namespace Companions.Controllers
{
    public class ChatController : Controller
    {
        public ActionResult Index()
        {
            try
            {
                Updates _u = new Updates();
                ViewBag.N = _u.CheckNotifications(User.Identity.Name);
                ViewBag.M = _u.CheckMessages(User.Identity.Name);

                string sqlExpression = $"SELECT User2,NR1 FROM Dialogue WHERE (User1='{User.Identity.Name}') UNION SELECT User1,NR2 FROM Dialogue WHERE (User2 = '{User.Identity.Name}');";
                int count = 0;
                List<string> u = new List<string>();
                List<string> l = new List<string>();
                using (SqlConnection connection = new SqlConnection(Database.connectionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand(sqlExpression, connection);
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            if (reader.GetBoolean(1))
                            {
                                string item = reader.GetString(0);
                                u.Add(item);
                                count++;
                            }
                            else
                            {
                                string item = reader.GetString(0);
                                l.Add(item);
                            }
                        }
                    }
                    reader.Close();
                }



                ViewBag.u = u;
                ViewBag.count = count;
                return View(l);
            }
            catch
            {
                Response.Redirect("/Error/Exception");
                return View();
            }
        }

        public ActionResult New()
        { 
            return View();
        }

        public ActionResult im(string user)
        {
            try { 
            if (!User.Identity.IsAuthenticated)
            {
                Response.Redirect("/Chat");
                return Content("");
            }
            ViewBag.User = user;
            string sqlExpression = $"UPDATE Messenger SET Viewed=1 " +
                            $"WHERE User1 = '{user}' AND User2 = '{User.Identity.Name}';";

            using (SqlConnection connection = new SqlConnection(Database.connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(sqlExpression, connection);
                SqlDataReader reader = command.ExecuteReader();
            }

            //1 - не прочитано , 0 - прочитано
            //NR1 - польбзователь 1 не прочитал сообщение п. 2
            //NR2 - польбзователь 2 не прочитал сообщение п. 1

            sqlExpression = $"UPDATE Dialogue SET NR2=0 " +
                            $"WHERE (User1 = '{user}' AND User2 = '{User.Identity.Name}');" +
                            $"UPDATE Dialogue SET NR1=0 " +
                            $"WHERE (User2='{user}' AND User1='{User.Identity.Name}');";


            using (SqlConnection connection = new SqlConnection(Database.connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(sqlExpression, connection);
                SqlDataReader reader = command.ExecuteReader();
            }

            sqlExpression = $"SELECT * FROM Messenger WHERE (User1='{User.Identity.Name}' AND User2='{user}') OR (User2='{User.Identity.Name}' AND User1 = '{user}') ORDER BY MessageID DESC;";
            List<ChatModel> l = new List<ChatModel>();

            using (SqlConnection connection = new SqlConnection(Database.connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(sqlExpression, connection);
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows) // если есть данные
                {
                    while (reader.Read())
                    {
                        ChatModel item = new ChatModel();
                        
                        item.User1 = reader.GetString(1);
                        item.User2 = reader.GetString(2);
                        item.DateTime = reader.GetDateTime(3).ToString();
                        item.Viewed = reader.GetBoolean(4);
                        item.Message = reader.GetString(5);

                        l.Add(item);
                    }
                }
                reader.Close();
            }

            return View(l) ;
            }
            catch
            {
                Response.Redirect("/Error/Exception");
                return View();
            }
        }

        [HttpPost]
        public ActionResult SendMessage(string User1, string User2, string Viewed, string Message)
        {
            try { 
                string sqlExpression = $"INSERT INTO Messenger " +
                    $"VALUES ('{User1}', '{User2}', '{DateTime.Now}', {Viewed}, N'{Message}');";

                using (SqlConnection connection = new SqlConnection(Database.connectionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand(sqlExpression, connection);
                    SqlDataReader reader = command.ExecuteReader();
                }

                // 1 - отправитель
                // 2 - получатель

                sqlExpression = $"UPDATE Dialogue SET NR1=0,NR2=1 " +
                                $"WHERE User1 = '{User1}' AND User2 = '{User2}';" +
                                $"UPDATE Dialogue SET NR1=1,NR2=0 " +
                                $"WHERE User2 = '{User1}' AND User1 = '{User2}';";

                using (SqlConnection connection = new SqlConnection(Database.connectionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand(sqlExpression, connection);
                    SqlDataReader reader = command.ExecuteReader();
                }

                Response.Redirect("/Chat/im?user=" + User2);
                return View();
            }
            catch
            {
                Response.Redirect("/Error/Exception");
                return View();
            }
        }
    }
}
