﻿using Companions.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Microsoft.Data.SqlClient;
using System;
using System.Data;
using System.Threading.Tasks;
using Companions.Models.Database;

namespace Companions.Controllers
{
    public class NotifController : Controller
    {
        public ActionResult Index()
        {
            try
            {
                string sqlExpression = $"SELECT * FROM Notifications WHERE UserID='{User.Identity.Name}' ORDER BY MessageID DESC;";

                List<string> l = new List<string>();


                using (SqlConnection connection = new SqlConnection(Database.connectionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand(sqlExpression, connection);
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string item;
                            item = reader.GetString(2);
                            if (!reader.GetBoolean(3)) item = "(*) " + item;
                            l.Add(item);
                        }
                    }

                    reader.Close();
                }

                sqlExpression = $"UPDATE Notifications SET Viewed=1 WHERE UserID='{User.Identity.Name}'";

                using (SqlConnection connection = new SqlConnection(Database.connectionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand(sqlExpression, connection);
                    SqlDataReader reader = command.ExecuteReader();

                    reader.Close();
                }
                return View(l);
            }

            catch
            {
                Response.Redirect("/Error/Exception");
                return View();
            }

        }
    }
}
