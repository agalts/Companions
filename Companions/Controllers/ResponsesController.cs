﻿using Companions.Models;
using Companions.Models.Updates;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Microsoft.Data.SqlClient;
using System;
using System.Data;
using System.Threading.Tasks;
using Companions.Models.Updates;
using Companions.Models.Database;


namespace Companions.Controllers
{
    public class ResponsesController : Controller
    {
        public ActionResult Index()
        {
            Updates u = new Updates();
            ViewBag.N = u.CheckNotifications(User.Identity.Name);
            ViewBag.M = u.CheckMessages(User.Identity.Name);

            

            string sqlExpression = $"SELECT * FROM Responses WHERE UserResponsed='{User.Identity.Name}'";

            List<Response> l = new List<Response>();

            try
            {

                using (SqlConnection connection = new SqlConnection(Database.connectionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand(sqlExpression, connection);
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows) // если есть данные
                    {
                        while (reader.Read())
                        {
                            Response item = new Response();
                            item.idResponse = reader.GetInt32(0);
                            item.IdSuggestion = reader.GetInt32(1);
                            item.UserSend = reader.GetString(2);
                            if (reader.IsDBNull(4))
                            {
                                item.Viewed = false;
                                item.Verdict = false;
                            }

                            else
                            {
                                item.Viewed = true;
                                item.Verdict = reader.GetBoolean(4);
                            }
                            item.Count = reader.GetInt32(5);
                            item.OnlyThere = reader.GetBoolean(6);
                            item.OnlyBack = reader.GetBoolean(7);
                            item.ThereBack = reader.GetBoolean(8);
                            item.sugMoney = reader.GetInt32(9);
                            item.Message = reader.GetString(10);


                            sqlExpression = $"SELECT * FROM Suggestions WHERE Id={item.IdSuggestion};";

                            using (SqlConnection SecondConnection = new SqlConnection(Database.connectionString))
                            {
                                SecondConnection.Open();

                                SqlCommand scommand = new SqlCommand(sqlExpression, SecondConnection);
                                SqlDataReader sreader = scommand.ExecuteReader();

                                if (sreader.HasRows)
                                {
                                    while (sreader.Read())
                                    {
                                        if (sreader.GetValue(3) != DBNull.Value) item.Departure = sreader.GetString(3);
                                        if (sreader.GetValue(5) != DBNull.Value) item.DepartureDate = sreader.GetDateTime(5).ToString();
                                        if (sreader.GetValue(4) != DBNull.Value) item.Destination = sreader.GetString(4);
                                        if (sreader.GetValue(6) != DBNull.Value) item.DestinationDate = sreader.GetDateTime(6).ToString();
                                    }
                                }

                                sreader.Close();
                            }

                            l.Add(item);

                        }
                    }
                    reader.Close();
                }



                return View(l);
            }


            catch
            {
                Response.Redirect("/Error/Exception");
                return View();
            }
        }

        public ActionResult My()
        {
            Updates u = new Updates();
            ViewBag.N = u.CheckNotifications(User.Identity.Name);
            ViewBag.M = u.CheckMessages(User.Identity.Name);

            try
            {

                
                List<Response> l = new List<Response>();
                string sqlExpression = $"SELECT * FROM Suggestions WHERE UserPublished='{User.Identity.Name}';";
                using (SqlConnection IDConnection = new SqlConnection(Database.connectionString))
                {
                    IDConnection.Open();

                    SqlCommand idcommand = new SqlCommand(sqlExpression, IDConnection);
                    SqlDataReader idreader = idcommand.ExecuteReader();

                    if (idreader.HasRows)
                    {
                        while (idreader.Read())
                        {
                            sqlExpression = $"SELECT * FROM Responses WHERE IdSuggestion={idreader.GetInt32(0)}";

                            using (SqlConnection connection = new SqlConnection(Database.connectionString))
                            {
                                connection.Open();

                                SqlCommand command = new SqlCommand(sqlExpression, connection);
                                SqlDataReader reader = command.ExecuteReader();

                                if (reader.HasRows) // если есть данные
                                {
                                    while (reader.Read())
                                    {
                                        Response item = new Response();
                                        item.idResponse = reader.GetInt32(0);
                                        item.IdSuggestion = reader.GetInt32(1);
                                        item.UserSend = reader.GetString(2);
                                        if (reader.IsDBNull(4))
                                        {
                                            item.Viewed = false;
                                            item.Verdict = false;
                                        }

                                        else
                                        {
                                            item.Viewed = true;
                                            item.Verdict = reader.GetBoolean(4);
                                        }
                                        item.Count = reader.GetInt32(5);
                                        item.OnlyThere = reader.GetBoolean(6);
                                        item.OnlyBack = reader.GetBoolean(7);
                                        item.ThereBack = reader.GetBoolean(8);
                                        item.sugMoney = reader.GetInt32(9);
                                        item.Message = reader.GetString(10);


                                        sqlExpression = $"SELECT * FROM Suggestions WHERE Id={item.IdSuggestion};";

                                        using (SqlConnection SecondConnection = new SqlConnection(Database.connectionString))
                                        {
                                            SecondConnection.Open();

                                            SqlCommand scommand = new SqlCommand(sqlExpression, SecondConnection);
                                            SqlDataReader sreader = scommand.ExecuteReader();

                                            if (sreader.HasRows)
                                            {
                                                while (sreader.Read())
                                                {
                                                    if (sreader.GetValue(3) != DBNull.Value) item.Departure = sreader.GetString(3);
                                                    if (sreader.GetValue(5) != DBNull.Value) item.DepartureDate = sreader.GetDateTime(5).ToString();
                                                    if (sreader.GetValue(4) != DBNull.Value) item.Destination = sreader.GetString(4);
                                                    if (sreader.GetValue(6) != DBNull.Value) item.DestinationDate = sreader.GetDateTime(6).ToString();
                                                }
                                            }

                                            sreader.Close();
                                        }

                                        l.Add(item);

                                    }
                                }
                                reader.Close();
                            }
                        }
                    }

                    idreader.Close();
                }
                return View(l);
            }


            catch
            {
                Response.Redirect("/Error/Exception");
                return View();
            }
        }

        public class arguments
        {
            public int id;
            public bool Verdict;
        }


        [HttpPost]
        public ActionResult Approve(int id) // одобрить заявку
        {
            try
            {

                
                string sqlExpression = $"UPDATE Responses " +
                    $"SET Verdict=1 " +
                    $"WHERE idResponse={id}";

                using (SqlConnection connection = new SqlConnection(Database.connectionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand(sqlExpression, connection);
                    SqlDataReader reader = command.ExecuteReader();
                }


                sqlExpression = $"SELECT * FROM Responses " +
                    $"WHERE idResponse={id}";

                string UID = "";
                int count = 0;
                int sID = 0;

                using (SqlConnection connection = new SqlConnection(Database.connectionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand(sqlExpression, connection);
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows) // если есть данные
                    {
                        while (reader.Read())
                        {
                            UID = reader.GetString(2);
                            count = reader.GetInt32(5);
                            sID = reader.GetInt32(1);
                        }
                    }
                }


                sqlExpression = $"SELECT * FROM Suggestions " +
                    $"WHERE Id={sID}";

                int aS = 0;

                using (SqlConnection connection = new SqlConnection(Database.connectionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand(sqlExpression, connection);
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                        while (reader.Read())
                            aS = reader.GetInt32(8);
                }


                sqlExpression = $"UPDATE Suggestions SET AlreadySentApplication={aS + count} " +
                    $"WHERE Id={sID}";


                using (SqlConnection connection = new SqlConnection(Database.connectionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand(sqlExpression, connection);
                    SqlDataReader reader = command.ExecuteReader();
                }


                sqlExpression = $"INSERT INTO Notifications " +
                    $"VALUES ('{UID}', N'Пора собирать чемоданы - поездка состоится!\nДополнительная информация доступна в разделе Отклики.', 0);";

                using (SqlConnection connection = new SqlConnection(Database.connectionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand(sqlExpression, connection);
                    SqlDataReader reader = command.ExecuteReader();
                }

                Response.Redirect("/Responses/my");

                return View();
            }


            catch
            {
                Response.Redirect("/Error/Exception");
                return View();
            }
        }

        [HttpPost]
        public ActionResult Deflect(int id) // отклонить заявку
        {
            try
            {
                
                string sqlExpression = $"UPDATE Responses " +
                    $"SET Verdict=0 " +
                    $"WHERE idResponse={id}";

                using (SqlConnection connection = new SqlConnection(Database.connectionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand(sqlExpression, connection);
                    SqlDataReader reader = command.ExecuteReader();
                }



                sqlExpression = $"SELECT * FROM Responses " +
                    $"WHERE idResponse={id}";

                string UID = "";

                using (SqlConnection connection = new SqlConnection(Database.connectionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand(sqlExpression, connection);
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows) // если есть данные
                    {
                        while (reader.Read())
                        {
                            UID = reader.GetString(2);
                        }
                    }
                }

                sqlExpression = $"INSERT INTO Notifications " +
                    $"VALUES ('{UID}', N'Вашу заявку на поездку отклонили :(\nДополнительная информация доступна в разделе Отклики.', 0);";

                using (SqlConnection connection = new SqlConnection(Database.connectionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand(sqlExpression, connection);
                    SqlDataReader reader = command.ExecuteReader();
                }

                Response.Redirect("/Responses/my");

                return View();
            }


            catch
            {
                Response.Redirect("/Error/Exception");
                return View();
            }
        }
    }
}
