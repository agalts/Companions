﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Companions.Models;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;

using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

using System.Security.Claims;

using Microsoft.Data.SqlClient;
using System;
using System.Data;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Companions.Models.Updates;
using Companions.Models.Database;

namespace Companions.Controllers
{
    public class ClientController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create()
        {
            return View();
        }
        public ActionResult done()
        {
            return View();
        }

        [HttpPost]
        public ActionResult createSuggestion(string departure, string arrival, string ddate, string edate, string count, bool ifdriver, bool ifpassanger, bool smoking, bool pet, bool dialogue, string cost, string auto)
        {
                int _smoking = smoking ? 1 : 0;
                int _pet = pet ? 1 : 0;
                int _dialogue = dialogue ? 1 : 0;
                int _pass;
                if (ifdriver && !ifpassanger) _pass = 0;
                else _pass = 1;
                string connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=CompanionDB;TrustServerCertificate=true;Integrated Security=SSPI";
                string sqlExpression = $"INSERT INTO Suggestions" +
                    $" VALUES (N'{User.Identity.Name}', {_pass}, N'{departure}', N'{arrival}', '{ddate}', '{edate}', {count}, 0, '{auto}', {cost}, {_smoking}, {_pet}, {_dialogue});";
                List<Suggestions> l = new List<Suggestions>();

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand(sqlExpression, connection);
                    SqlDataReader reader = command.ExecuteReader();
                }
                Response.Redirect("/Client/SuccessfullySugegstion");
                return View();
        }

        public ActionResult SuccessfullySugegstion()
        {
            return View();
        }

        public ActionResult Respond(int id)
        {
            ViewBag.Id = id;
            ViewBag.UserID = User.Identity.Name;

            string sqlExpression = $"SELECT * FROM Suggestions WHERE Id={id}";
            Suggestions item = new Suggestions();
            try
            {
                using (SqlConnection connection = new SqlConnection(Database.connectionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand(sqlExpression, connection);
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows) // если есть данные
                    {
                        while (reader.Read())
                        {
                            item.id = Convert.ToInt32(reader.GetValue(0));
                            item.departureCity = reader.GetValue(3).ToString();
                            item.arrivalCity = reader.GetValue(4).ToString();
                            if (reader.GetValue(5) != DBNull.Value) item.ArrivalDestinationDate = reader.GetValue(5).ToString();
                            if (reader.GetValue(6) != DBNull.Value) item.ArrivalDepartureDate = reader.GetValue(6).ToString();
                            if (reader.GetValue(7) != DBNull.Value) item.CountOfCompanions = Convert.ToInt32(reader.GetValue(7));
                            item.UserPublished = reader.GetValue(1).ToString();
                            if (reader.GetValue(10) != DBNull.Value) item.TravelCost = Convert.ToInt32(reader.GetValue(10));
                            if (reader.GetValue(8) != DBNull.Value) item.AlreadySentApplication = Convert.ToInt32(reader.GetValue(8));
                            if (reader.GetValue(9) != DBNull.Value) item.Automobile = reader.GetValue(9).ToString();

                            if (reader.GetValue(11) != DBNull.Value) item.smoking = reader.GetBoolean(11);
                            if (reader.GetValue(12) != DBNull.Value) item.pet = reader.GetBoolean(12);
                            if (reader.GetValue(13) != DBNull.Value) item.dialogue = reader.GetBoolean(13);

                            if (Convert.ToBoolean(reader.GetValue(2)))
                                item.Traveller = "Попутчик";
                            else
                                item.Traveller = "Путешественник";
                        }
                    }

                    reader.Close();
                    ViewBag.l = item;
                    ViewBag.User = item.UserPublished;
                }

                return View();
            }
            catch
            {
                Response.Redirect("/Error/Exception");

                return View();
            }
        }

        public ActionResult Send(int id, int free)
        {
            ViewBag.Id = id;
            ViewBag.UserID = User.Identity.Name;
            ViewBag.Free = free;

            return View();
        }

        [HttpPost]
        public void SendRequest(string id, string count, string option, string money, string message, string free)
        {
            int all = 0, there = 0, back = 0;
            if (option == "all") all = 1;
            else if (option == "there") there = 1;
            else if (option == "back") back = 1;

            if (Convert.ToInt32(count) > Convert.ToInt32(free))
            {
                Response.Redirect($"/Client/errormanyfellow");
            }

            else
            {
                try
                {
                    string sqlExpression = $"INSERT INTO Responses (idSuggestion, UserResponsed, Date, Verdict, Count, OnlyThere, OnlyBack, ThereBack, SuggestedMoney, Message)" +
                        $"VALUES ({id}, '{User.Identity.Name}', NULL, NULL, {count}, {there}, {back}, {all}, {money}, N'{message}');";
                    //return Content(sqlExpression);
                    List<Suggestions> l = new List<Suggestions>();

                    using (SqlConnection connection = new SqlConnection(Database.connectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand(sqlExpression, connection);
                        SqlDataReader reader = command.ExecuteReader();
                    }


                    sqlExpression = $"SELECT * FROM Suggestions " +
                        $"WHERE Id={id}";

                    string UID = "";
                    using (SqlConnection connection = new SqlConnection(Database.connectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand(sqlExpression, connection);
                        SqlDataReader reader = command.ExecuteReader();

                        if (reader.HasRows) // если есть данные
                        {
                            while (reader.Read())
                            {
                                UID = reader.GetString(1);
                            }
                        }
                    }

                    sqlExpression = $"INSERT INTO Notifications " +
                        $"VALUES ('{UID}', N'На Вашу поездку пришёл новый отклик. Скорее посмотрите, что там!', 0);";

                    using (SqlConnection connection = new SqlConnection(Database.connectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand(sqlExpression, connection);
                        SqlDataReader reader = command.ExecuteReader();
                    }


                    Response.Redirect($"/Client/Successfully?user={UID}");
                }
                catch
                {
                    Response.Redirect("/Error/Exception");
                }
            }
        }


        public ActionResult errormanyfellow()
        {
            return View();
        }

        public ActionResult Edit(parametrs pr)
        {
            //ViewBag.User = User.Identity.GetUserId();
            ViewBag.IfTourist = (pr.ift == 1 ? true : false);
            ViewBag.Id = pr.id;
            ViewBag.UserId = pr.uid;

            var user = User.Identity.Name;

            if (user == pr.uid)
            {
                try
                {
                    string sqlExpression = $"SELECT * FROM Suggestions WHERE Id={pr.id}";
                    Suggestions item = new Suggestions();
                    using (SqlConnection connection = new SqlConnection(Database.connectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand(sqlExpression, connection);
                        SqlDataReader reader = command.ExecuteReader();

                        if (reader.HasRows) // если есть данные
                        {
                            while (reader.Read())
                            {
                                item.id = Convert.ToInt32(reader.GetValue(0));
                                item.departureCity = reader.GetValue(3).ToString();
                                item.arrivalCity = reader.GetValue(4).ToString();
                                if (reader.GetValue(5) != DBNull.Value) item.ArrivalDestinationDate = reader.GetValue(5).ToString();
                                if (reader.GetValue(6) != DBNull.Value) item.ArrivalDepartureDate = reader.GetValue(6).ToString();
                                if (reader.GetValue(7) != DBNull.Value) item.CountOfCompanions = Convert.ToInt32(reader.GetValue(7));
                                item.UserPublished = reader.GetValue(1).ToString();
                                if (reader.GetValue(10) != DBNull.Value) item.TravelCost = Convert.ToInt32(reader.GetValue(10));
                                if (reader.GetValue(8) != DBNull.Value) item.AlreadySentApplication = Convert.ToInt32(reader.GetValue(8));
                                if (reader.GetValue(9) != DBNull.Value) item.Automobile = reader.GetValue(9).ToString();

                                if (Convert.ToBoolean(reader.GetValue(2)))
                                    item.Traveller = "Попутчик";
                                else
                                    item.Traveller = "Путешественник";
                            }
                        }

                        reader.Close();
                    }
                    ViewBag.l = item;

                    return View();
                }

                catch
                {
                    Response.Redirect("/Error/Exception");

                    return View();
                }
            }
            else
            {
                return RedirectToAction("Index", "Error");
            }
        }

        public ActionResult Delete(string id, string pruser)
        {
            var user = User.Identity.Name;

            if (user == pruser)
            {
                try
                {
                    string sqlExpression = $"DELETE FROM Suggestions WHERE Id={id}";

                    using (SqlConnection connection = new SqlConnection(Database.connectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand(sqlExpression, connection);
                        SqlDataReader reader = command.ExecuteReader();
                        reader.Close();
                    }

                    sqlExpression = $"DELETE FROM Responses WHERE idSuggestion={id}";

                    using (SqlConnection connection = new SqlConnection(Database.connectionString))
                    {
                        connection.Open();

                        SqlCommand command = new SqlCommand(sqlExpression, connection);
                        SqlDataReader reader = command.ExecuteReader();
                        reader.Close();
                    }


                    return View();
                }
                catch
                {
                    Response.Redirect("/Error/Exception");

                    return View();
                }
            }
            else
            {
                return RedirectToAction("Index", "Error");
            }
        }


        public class parametrs
        {
            public int id { get; set; }
            public int ift { get; set; }
            public string uid { get; set; }

        }

        [HttpPost]
        public void submitchanges(string departure, string arrival, string Cty1, string Cty2, string cms, string auto, string cost, string id)
        {
            string sqlExpression = $"UPDATE Suggestions SET departureCity=N'{departure}', " +
                $"arrivalCity=N'{arrival}', " +
                $"ArrivalDestinationDate='{Cty1}', " +
                $"ArrivalDepartureDate='{Cty2}', " +
                $"CountOfCompanions='{cms}', " +
                $"Automobile=N'{auto}', " +
                $"TravelCost='{cost}' " +
                $"WHERE Id={id}";

            try
            {
                using (SqlConnection connection = new SqlConnection(Database.connectionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand(sqlExpression, connection);
                    SqlDataReader reader = command.ExecuteReader();
                }
                Response.Redirect("/List/Index");
            }
            catch
            {
                Response.Redirect("/Error/Exception");
            }
        }


        public class adr
        {
            public string departure;
            public string destination;
        }

        public class FindOptions
        {
            public bool Who;
            public adr a;
        }

        FindOptions findOptions = new FindOptions();

        public ActionResult Find()
        {
            return View();
        }

        public ActionResult Adress(bool Who)
        {
            findOptions.Who = Who;
            return View();
        }

        public ActionResult successfully(string user)
        {
            ViewBag.l = user;

            return View();
        }

        public ActionResult letsgo()
        {
            return View();
        }

        public ActionResult trips()
        {
            Updates u = new Updates();
            ViewBag.N = u.CheckNotifications(User.Identity.Name);
            ViewBag.M = u.CheckMessages(User.Identity.Name);

            try
            {
                string sqlExpression = $"SELECT * FROM Suggestions WHERE UserPublished='{User.Identity.Name}'";
                //return Content(sqlExpression);
                List<Suggestions> l = new List<Suggestions>();

                using (SqlConnection connection = new SqlConnection(Database.connectionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand(sqlExpression, connection);
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows) // если есть данные
                    {
                        while (reader.Read())
                        {
                            Suggestions item = new Suggestions();
                            item.id = Convert.ToInt32(reader.GetValue(0));
                            item.departureCity = reader.GetValue(3).ToString();
                            item.arrivalCity = reader.GetValue(4).ToString();
                            if (reader.GetValue(5) != DBNull.Value) item.ArrivalDestinationDate = reader.GetValue(5).ToString();
                            if (reader.GetValue(6) != DBNull.Value) item.ArrivalDepartureDate = reader.GetValue(6).ToString();
                            if (reader.GetValue(7) != DBNull.Value) item.CountOfCompanions = Convert.ToInt32(reader.GetValue(7));
                            item.UserPublished = reader.GetValue(1).ToString();
                            if (reader.GetValue(10) != DBNull.Value) item.TravelCost = Convert.ToInt32(reader.GetValue(10));
                            if (reader.GetValue(8) != DBNull.Value) item.AlreadySentApplication = Convert.ToInt32(reader.GetValue(8));
                            if (reader.GetValue(9) != DBNull.Value) item.Automobile = reader.GetValue(9).ToString();

                            if (Convert.ToBoolean(reader.GetValue(2)))
                                item.Traveller = "Попутчик";
                            else
                                item.Traveller = "Путешественник";

                            l.Add(item);
                        }
                    }
                    reader.Close();
                    return View(l);
                }
            }
            catch
            {
                Response.Redirect("/Error/Exception");
                return View();
            }
        }


        [HttpPost]
        public ActionResult Idea(string departure, string arrival)
        {
            ViewData["Arrival"] = arrival;
            int value; if (findOptions.Who) value = 1; else value = 0;
            string sqlExpression = $"SELECT * FROM Suggestions WHERE departureCity=N'{departure}' AND arrivalCity=N'{arrival}' AND Traveller={value};";

            List<Suggestions> l = new List<Suggestions>();

            try
            {

                using (SqlConnection connection = new SqlConnection(Database.connectionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand(sqlExpression, connection);
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows) // если есть данные
                    {
                        while (reader.Read())
                        {
                            Suggestions item = new Suggestions();
                            item.id = Convert.ToInt32(reader.GetValue(0));
                            item.departureCity = reader.GetValue(3).ToString();
                            item.arrivalCity = reader.GetValue(4).ToString();
                            if (reader.GetValue(5) != DBNull.Value) item.ArrivalDestinationDate = reader.GetValue(5).ToString();
                            if (reader.GetValue(6) != DBNull.Value) item.ArrivalDepartureDate = reader.GetValue(6).ToString();
                            if (reader.GetValue(7) != DBNull.Value) item.CountOfCompanions = Convert.ToInt32(reader.GetValue(7));
                            item.UserPublished = reader.GetValue(1).ToString();
                            if (reader.GetValue(10) != DBNull.Value) item.TravelCost = Convert.ToInt32(reader.GetValue(10));
                            if (reader.GetValue(8) != DBNull.Value) item.AlreadySentApplication = Convert.ToInt32(reader.GetValue(8));
                            if (reader.GetValue(9) != DBNull.Value) item.Automobile = reader.GetValue(9).ToString();

                            if (Convert.ToBoolean(reader.GetValue(2)))
                                item.Traveller = "Попутчик";
                            else
                                item.Traveller = "Путешественник";

                            l.Add(item);
                        }
                    }


                    reader.Close();
                    //return Content(ToOut);
                    return View(l);
                }
            }

            catch
            {
                Response.Redirect("/Error/Exception");
                return View();
            }
        }

        [HttpPost]
        public ActionResult AllResults(string arrival)
        {
            ViewData["Arrival"] = arrival;

            int value; if (findOptions.Who) value = 1; else value = 0;
            string sqlExpression = $"SELECT * FROM Suggestions WHERE arrivalCity=N'{arrival}' AND Traveller={value};";
            //return Content(sqlExpression);
            List<Suggestions> l = new List<Suggestions>();

            try
            {

                using (SqlConnection connection = new SqlConnection(Database.connectionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand(sqlExpression, connection);
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows) // если есть данные
                    {
                        while (reader.Read())
                        {
                            Suggestions item = new Suggestions();
                            item.id = Convert.ToInt32(reader.GetValue(0));
                            item.departureCity = reader.GetValue(3).ToString();
                            item.arrivalCity = reader.GetValue(4).ToString();
                            if (reader.GetValue(5) != DBNull.Value) item.ArrivalDestinationDate = reader.GetValue(5).ToString();
                            if (reader.GetValue(6) != DBNull.Value) item.ArrivalDepartureDate = reader.GetValue(6).ToString();
                            if (reader.GetValue(7) != DBNull.Value) item.CountOfCompanions = Convert.ToInt32(reader.GetValue(7));
                            item.UserPublished = reader.GetValue(1).ToString();
                            if (reader.GetValue(10) != DBNull.Value) item.TravelCost = Convert.ToInt32(reader.GetValue(10));
                            if (reader.GetValue(8) != DBNull.Value) item.AlreadySentApplication = Convert.ToInt32(reader.GetValue(8));
                            if (reader.GetValue(9) != DBNull.Value) item.Automobile = reader.GetValue(9).ToString();

                            if (Convert.ToBoolean(reader.GetValue(2)))
                                item.Traveller = "Попутчик";
                            else
                                item.Traveller = "Путешественник";

                            l.Add(item);
                        }
                    }


                    reader.Close();
                    return View(l);
                }
            }
            catch
            {
                Response.Redirect("/Error/Exception");

                return View();
            }
        }
    }
}
