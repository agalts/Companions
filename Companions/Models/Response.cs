﻿namespace Companions.Models
{
    public class Response
    {
        public int idResponse;
        public int IdSuggestion;
        public string UserSend;
        public int Count;
        public string option;
        public string Message;
        public int sugMoney;
        public bool Verdict;

        public bool OnlyThere;
        public bool OnlyBack;
        public bool ThereBack;

        public bool Viewed;

        public string Departure;
        public string DepartureDate;
        public string Destination;
        public string DestinationDate;
        public string UserAppl;

    }
}
