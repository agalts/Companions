﻿using System.ComponentModel.DataAnnotations;

namespace Companions.Models
{
    public class edit
    {
        [Display(Name = "Откуда")]
        public string departure { get; set; }


        [Display(Name = "Куда")]
        public string arrival { get; set; }


        [Display(Name = "Туда")]
        public string Cty1 { get; set; }


        [Display(Name = "Обратно")]
        public string Cty2 { get; set; }


        [Display(Name = "Количество мест")]
        public string cms { get; set; }


        [Display(Name = "Автомобиль")]
        public string auto { get; set; }


        [Display(Name = "Цена одной поездки")]
        public string cost { get; set; }
    }
}
