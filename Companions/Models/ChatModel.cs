﻿namespace Companions.Models
{
    public class ChatModel
    {
        public int    id;
        public string User1;
        public string User2;
        public string DateTime;
        public bool   Viewed;
        public string Message;
    }
}
