﻿using Companions.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Microsoft.Data.SqlClient;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Security;
using System.Security.Claims;

namespace Companions.Models.Updates
{
    public class Updates
    {
        public bool CheckNotifications(string UIN)
        {
            string connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=CompanionDB;TrustServerCertificate=true;Integrated Security=SSPI";

            string sqlExpression = $"SELECT * FROM Notifications WHERE UserID='{UIN}'";

            int count = 0;
            try
            {

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand(sqlExpression, connection);
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                        while (reader.Read())
                            if (!reader.GetBoolean(3))
                                count++;

                    reader.Close();
                }

                return (count > 0);
            }
            catch
            {
                return false;
            }
        }
        public bool CheckMessages(string UIN)
        {
            string connectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=CompanionDB;TrustServerCertificate=true;Integrated Security=SSPI";

            string sqlExpression = $"SELECT User2,NR1 FROM Dialogue WHERE (User1='{UIN}') UNION SELECT User1,NR2 FROM Dialogue WHERE (User2 = '{UIN}');";
            int count = 0;
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand(sqlExpression, connection);
                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                        while (reader.Read())
                            if (reader.GetBoolean(1))
                                count++;

                    reader.Close();
                }

                return (count > 0);
            }
            catch
            {
                return false;
            }
        }
    }
}
