﻿using System.Reflection.PortableExecutable;

namespace Companions.Models
{
    public class Suggestions
    {
        public int id { get; set; }
        public string departureCity { get; set; }
        public string arrivalCity { get; set; }
        public string ArrivalDestinationDate { get; set; }
        public string ArrivalDepartureDate { get; set; }
        public int CountOfCompanions { get; set; }
        public string UserPublished { get; set; }
        public int TravelCost { get; set; }
        public int AlreadySentApplication { get; set; }
        public string Automobile { get; set; }
        public string Traveller { get; set; }

        public bool passanger;
        public bool driver;

        public bool option;
        public bool smoking;
        public bool pet;
        public bool dialogue;
    }
}
